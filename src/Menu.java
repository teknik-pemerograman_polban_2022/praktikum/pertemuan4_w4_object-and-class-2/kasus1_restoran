public class Menu {
    private String nama_makanan;
    private double harga_makanan;
    private int stok;

    public Menu() {
        nama_makanan = "";
        harga_makanan = 0;
        stok = 0;
    }

    public void NewMenu(String nama, double harga, int stok) {
        setNama_makanan(nama);
        setHarga_makanan(harga);
        setStok(stok);
    }

    public String getNama_makanan() {
        return this.nama_makanan;
    }

    public void setNama_makanan(String nama_makanan) {
        this.nama_makanan = nama_makanan;
    }

    public double getHarga_makanan() {
        return this.harga_makanan;
    }

    public void setHarga_makanan(double harga_makanan) {
        this.harga_makanan = harga_makanan;
    }

    public int getStok() {
        return this.stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public boolean isOutOfStock() {
        if (stok == 0) {
            return true;
        } else {
            return false;
        }
    }

}
