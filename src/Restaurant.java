public class Restaurant {
    private Menu[] menus;
    private static byte id = -1;

    public byte getid() {
        return this.id;
    }

    // ! Always Keep Data private
    // ! Breakup classes that have too many responsibilities

    public Restaurant() {
        menus = new Menu[100];
        for (int i = 0; i < 100; i++) {
            menus[i] = new Menu();
        }
    }

    public void tambahMenuMakanan(String nama, double harga, int stok) {
        nextId();
        menus[id].NewMenu(nama, harga, stok);
    }

    public void tampilMenuMakanan() {
        for (int i = 0; i <= id; i++) {
            if (!menus[i].isOutOfStock()) {
                System.out.println(
                        menus[i].getNama_makanan() + "[" + menus[i].getStok() + "]" + "\tRp. "
                                + menus[i].getHarga_makanan());
            }
        }
    }

    public static void nextId() {
        id++;
    }
}
